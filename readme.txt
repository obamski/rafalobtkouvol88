Object pooling zosta� wykorzystany przy spawnowaniu pocisk�w - ma to w tym miejscu du�e znaczenie, ze wzgl�du na fakt mo�liwo�ci wykorzystania pocisk�w kt�re spe�ni�y ju� swoje zadanie.
W przypadku spawnowania wie�yczek object pooling r�wnie� mia�by sens, jednak nie jest to w tym wypadku tak istotne, gdy� istnieje pewna liczba wie�yczek, kt�re b�d� istnia�y na 
scenie w pewnym momencie w czasie. Skoro zatem musi w pewnym momencie istnie� 100 wie�yczek, to spawnuj�c je tylko na starcie (by nast�pnie u�y� poolingu) tylko przek�adamy 
maksymalne obci��enie procesora na start sceny, co mo�e skutkowa� spowolnieniem �adowania si� jej. Mogliby�my r�wnie� zespawnowa� 100 wie�yczek w edytorze (a nast�pnie
u�y� ich w puli), jednak takie dzia�aniejest moim zdaniem nieintuicyjne.