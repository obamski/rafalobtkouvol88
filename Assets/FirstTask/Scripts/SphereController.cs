﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    #region Public properties
    public const float CIRCLE_PATH_MARGIN = 0.2f;
    #endregion
    #region Serialized properties
    [SerializeField]
    private Transform pivot;
    #endregion
    #region Private properties
    private Vector3 screenPoint;
    private Vector3 offset;
    private Camera mainCamera;
    private Vector3 currentMousePosition;
    private Vector3 mousePositionWorldSpace;
    #endregion

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    #region Simple properties
    private float DistanceToPivot()
    {
        return Vector3.Distance(gameObject.transform.position, pivot.transform.position);
    }
    private bool DragIsOnCircleWithMargin()
    {
        return (Vector3.Distance(mousePositionWorldSpace, pivot.position) > (DistanceToPivot() - CIRCLE_PATH_MARGIN) && Vector3.Distance(mousePositionWorldSpace, pivot.position) < (DistanceToPivot() + CIRCLE_PATH_MARGIN));
    }
    #endregion
    #region Mouse movement
    void OnMouseDrag()
    {
        currentMousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        mousePositionWorldSpace = Camera.main.ScreenToWorldPoint(currentMousePosition);

        if (DragIsOnCircleWithMargin())
        {
            transform.position = mousePositionWorldSpace;
        }
        else
        {
            MappDragPositionOntoCirclePath();
        }
    }

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }
    #endregion

    void MappDragPositionOntoCirclePath()
    {
        Vector3 targetScreenPos = Camera.main.WorldToScreenPoint(pivot.position);
        targetScreenPos.z = 0;
        Vector3 targetToMouseDir = Input.mousePosition - targetScreenPos;
        Vector3 targetToMe = transform.position - pivot.position;
        targetToMe.z = 0;
        Vector3 newTargetToMe = Vector3.RotateTowards(targetToMe, targetToMouseDir, DistanceToPivot(), 0f);
        transform.position = pivot.position + DistanceToPivot() * newTargetToMe.normalized;
    }
}
