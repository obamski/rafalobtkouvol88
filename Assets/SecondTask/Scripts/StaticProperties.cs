﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticProperties : ScriptableObject
{

    public static float MIN_RANDOM_ROTATION_ANGLE = 15;
    public static float MAX_RANDOM_ROTATION_ANGLE = 45;
    public static float ROTATION_TIME_INTERVAL = 0.5f;
    public static float MIN_UNITS_BULLET_MOVEMENT = 1;
    public static float MAX_UNITS_BULLET_MOVEMENT = 4;
    public static float BULLET_MOVEMENT_UNITS_PER_SECOND = 4;
    public static int AMOUNT_OF_BULLETS_FOR_ONE_TURRET = 12;
    public static Color ACTIVE_TURRET_COLOR = new Color(255, 0, 0);
    public static Color WAITING_TURRET_COLOR = new Color(255, 255, 255);
    public static int MAX_TURRETS_ON_SCENE = 100;


}
