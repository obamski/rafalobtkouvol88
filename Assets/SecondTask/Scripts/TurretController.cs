﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{

    protected bool isInRotatingState;
    protected bool isInShootingState;
    [SerializeField]
    protected List<GameObject> bulletsPool;
    [SerializeField]
    protected GameObject bulletPrefab;
    [SerializeField]
    protected Transform shootPivot;
    private int shotBulletCounter;
    protected virtual void Start()
    {
        GetComponent<MeshRenderer>().material.color = StaticProperties.ACTIVE_TURRET_COLOR;
        isInRotatingState = true;
        isInShootingState = false;
        shotBulletCounter = 0;
        StartCoroutine(RotateTurretRandomlyWithInterval());
    }
    private void OnEnable()
    {

        SceneManager.OnMaxTurretsReached += StartShootingAgain;

    }
    protected void StartShootingAgain()
    {
        if (gameObject.activeSelf)
        {
            shotBulletCounter = 0;
            GetComponent<MeshRenderer>().material.color = StaticProperties.ACTIVE_TURRET_COLOR;
            StartCoroutine(RotateTurretRandomlyWithInterval());
        }
    }
    protected IEnumerator RotateTurretRandomlyWithInterval()
    {
        if (shotBulletCounter < StaticProperties.AMOUNT_OF_BULLETS_FOR_ONE_TURRET)
        {
            yield return new WaitForSeconds(StaticProperties.ROTATION_TIME_INTERVAL);
            gameObject.transform.Rotate(new Vector3(0, GetRandomAngle(), 0));
            ShootBullet();
            StartCoroutine(RotateTurretRandomlyWithInterval());
        }
        else
        {
            GetComponent<MeshRenderer>().material.color = StaticProperties.WAITING_TURRET_COLOR;
        }
    }
    protected float GetRandomAngle()
    {
        return Random.Range(StaticProperties.MIN_RANDOM_ROTATION_ANGLE, StaticProperties.MAX_RANDOM_ROTATION_ANGLE);
    }
    protected void ShootBullet()
    {
        shotBulletCounter++;
        GameObject bulletToShoot = FindInactiveBulletInPool();
        bulletToShoot.transform.SetParent(Camera.main.transform);
        bulletPrefab.transform.position = shootPivot.position;
        FindInactiveBulletInPool().SetActive(true);
    }
    protected GameObject FindInactiveBulletInPool()
    {
        foreach (GameObject bullet in bulletsPool)
        {
            if (!bullet.activeSelf)
            {
                bullet.transform.SetParent(shootPivot.transform);
                return bullet;
            }
        }
        return Instantiate(bulletPrefab, gameObject.transform);
    }
}
