﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public delegate void TurretAction();
    public static event TurretAction OnTurretSpawn;
    public static event TurretAction OnTurretDestroy;

    Vector3 randomTarget;
    [SerializeField]
    private GameObject turretClonePrefab;
    float timeToReachTarget;
    float step;

    private void OnEnable()
    {
        float randomYDestination = GetRandomYTarget();
        step = 0;
        randomTarget = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + randomYDestination, gameObject.transform.localPosition.z);
        timeToReachTarget = randomYDestination / StaticProperties.BULLET_MOVEMENT_UNITS_PER_SECOND;
    }
    float GetRandomYTarget()
    {
        return Random.Range(StaticProperties.MIN_UNITS_BULLET_MOVEMENT, StaticProperties.MAX_UNITS_BULLET_MOVEMENT);
    }
    private void FixedUpdate()
    {
        step += Time.deltaTime / timeToReachTarget;
        gameObject.transform.localPosition = Vector3.Lerp(gameObject.transform.localPosition, randomTarget, step);
        if (gameObject.transform.localPosition == randomTarget)
        {
            if (!SceneManager.isMaxTurretsOnScene)
            {
                GameObject spawnedTurret = Instantiate(turretClonePrefab);
                spawnedTurret.transform.position = gameObject.transform.position;
                if (OnTurretSpawn != null)
                    OnTurretSpawn.Invoke();
            }
            gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<TurretCloneController>() != null)
        {
            if (OnTurretDestroy != null)
                OnTurretDestroy.Invoke();
            other.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
