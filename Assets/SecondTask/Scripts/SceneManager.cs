﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneManager : MonoBehaviour
{
    public delegate void TurretNumberAction();
    public static event TurretNumberAction OnMaxTurretsReached;

    [SerializeField]
    private Text currentTurretsText;
    public int turretsOnScene;
    public static bool isMaxTurretsOnScene;

    private void Awake()
    {
        turretsOnScene = 0;
        isMaxTurretsOnScene = false;
    }
    private void OnEnable()
    {
        BulletController.OnTurretSpawn += AddTurret;
        BulletController.OnTurretDestroy += DeleteTurret;

    }
    public void AddTurret()
    {
        turretsOnScene++;
        if (turretsOnScene == StaticProperties.MAX_TURRETS_ON_SCENE)
        {
            isMaxTurretsOnScene = true;
            if (OnMaxTurretsReached != null)
                OnMaxTurretsReached.Invoke();
        }
        currentTurretsText.text = "Towers: " + turretsOnScene.ToString();
    }

    public void DeleteTurret()
    {
        turretsOnScene--;
        currentTurretsText.text = "Towers: " + turretsOnScene.ToString();
    }
}
