﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretCloneController : TurretController
{
    protected override void Start()
    {
        StartCoroutine(WaitAndStart());
    }
    IEnumerator WaitAndStart()
    {
        yield return new WaitForSeconds(6);
        base.Start();
    }
}
